## Example robots.txt file for stopping bots from flooding your log files
## if the 'alert watchdog' option is chosen in the link2page settings
##
## You can copy the lines below to your existing robots.txt file
## or just place this one in your websites root directory: 
## Ex. http://example.com/robots.txt
##
## You will need to edit the 2nd line to reflect your drupal root dir.

User-agent: *
Disallow: /YOUR_DRUPAL_ROOT_DIR/link2page
